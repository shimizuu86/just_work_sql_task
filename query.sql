-- Запрос количества книг на складах в каждом магазине

SELECT shop.name, SUM(count)
FROM shop
JOIN shops_books USING (shop_id)
JOIN book USING (book_id)
GROUP BY shop.name;

-- Запрос книг написанных каким либо автором

SELECT author.first_name, author.last_name, book.name
FROM author
JOIN authors_books USING (author_id)
JOIN book USING (book_id)
WHERE (author.first_name = 'Александр' AND author.last_name = 'Пушкин');