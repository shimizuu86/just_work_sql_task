CREATE DATABASE books_table;

USE books_table;

CREATE TABLE `book` (
	`book_id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	PRIMARY KEY (`book_id`)
);

CREATE TABLE `shop` (
	`shop_id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	`address` varchar(70) NOT NULL,
	PRIMARY KEY (`shop_id`)
);

CREATE TABLE `author` (
	`author_id` INT NOT NULL AUTO_INCREMENT,
	`first_name` varchar(50) NOT NULL,
	`last_name` varchar(50) NOT NULL,
	PRIMARY KEY (`author_id`)
);

CREATE TABLE `authors_books` (
	`author_id` INT NOT NULL,
	`book_id` INT NOT NULL
);

CREATE TABLE `shops_books` (
	`book_id` INT NOT NULL,
	`shop_id` INT NOT NULL,
	`count` INT NOT NULL
);

ALTER TABLE `authors_books` ADD CONSTRAINT `authors_books_fk0` FOREIGN KEY (`author_id`) REFERENCES `author`(`author_id`);

ALTER TABLE `authors_books` ADD CONSTRAINT `authors_books_fk1` FOREIGN KEY (`book_id`) REFERENCES `book`(`book_id`);

ALTER TABLE `shops_books` ADD CONSTRAINT `shops_books_fk0` FOREIGN KEY (`book_id`) REFERENCES `book`(`book_id`);

ALTER TABLE `shops_books` ADD CONSTRAINT `shops_books_fk1` FOREIGN KEY (`shop_id`) REFERENCES `shop`(`shop_id`);

